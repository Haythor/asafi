module ASAFI {
    requires org.apache.logging.log4j;
    requires org.apache.commons.text;
    requires java.curl;
    requires yamlbeans;

    exports main.java.org.unrealview;
    exports main.java.org.unrealview.model;
    exports main.java.org.unrealview.model.file;
    exports main.java.org.unrealview.model.schedule;
    exports main.java.org.unrealview.model.automation;
    exports main.java.org.unrealview.model.automation.object;
    exports main.java.org.unrealview.model.automation.search;
    exports main.java.org.unrealview.model.automation.task;
    exports main.java.org.unrealview.model.automation.task.browse;
    exports main.java.org.unrealview.model.automation.task.filter;
    exports main.java.org.unrealview.model.automation.task.insta;
}