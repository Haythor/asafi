/*
 * MainWindow.java                               date: 03/05/2020 19:23
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.vue.main;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class MainWindow {

    /** TODO describe this attribute */
    private Stage stage;

    public MainWindow() {

        stage = new Stage();

        BorderPane root = new BorderPane();
        root.setBackground(new Background(new BackgroundFill(Color.valueOf(
                "1d1d1d"), CornerRadii.EMPTY, Insets.EMPTY)));

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setHeight(900);
        stage.setWidth(1600);
        stage.setTitle("Bot Profile");
        stage.centerOnScreen();
        stage.initStyle(StageStyle.UNIFIED);
        stage.show();
    }
}