/*
 * StartWindow.java                              date: 03/05/2020 19:21
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.vue.start;

import jfxtras.styles.jmetro.JMetro;
import jfxtras.styles.jmetro.Style;
import main.java.org.unrealview.controller.ControllerStartWindow;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

/**
 * Welcome window allowing to ask the user's credentials to launch the main
 * application.
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class StartWindow {

    /** Stage of the different windows of the application. */
    private Stage stage;

    /** Internal pane containing interface elements. */
    private BorderPane innerRoot;

    /** Controller of the start window. */
    private ControllerStartWindow controllerStartWindow;

    /** Element allowing the user to choose among parked accounts. */
    private ChoiceBox<String> userChooser;

    /** Request fields for user account name. */
    private TextField nameField;

    /** Request fields for user account password. */
    private PasswordField passField;

    /** Button for starting the connection task. */
    private Button connectBtn;

    /** Connection status progress indication bar. */
    private ProgressBar connexionProgress;

    /** Text information to inform about the status of the connection. */
    private Text info;

    /** Button for closing the application. */
    private Button exitBtn;

    /** Transition time between two progress bar values. */
    public static final int TRANSITION_PROGRESS_TIME = 100;

    /**
     * Initializes and starts the window elements; starts the controller and
     * checks for account presence.
     *
     * @param stage main stage
     */
    public StartWindow(Stage stage) {

        this.stage = stage;

        /* To give a shadow effect around the window, all the external elements
         * are made transparent and the effect is applied to the internal pane.
         */
        innerRoot = new BorderPane();
        innerRoot.setBackground(new Background(new BackgroundFill(Color.valueOf(
                "1d1d1d"), CornerRadii.EMPTY, Insets.EMPTY)));

        BorderPane root = new BorderPane();
        root.setCenter(innerRoot);
        root.setPadding(new Insets(20));
        root.setEffect(new DropShadow(20, Color.rgb(0, 0, 0, 0.5)));
        root.setBackground(new Background(new BackgroundFill(Color.TRANSPARENT,
                                                             CornerRadii.EMPTY,
                                                             Insets.EMPTY)));

        JMetro theme = new JMetro();
        theme.setStyle(Style.DARK);
        theme.setParent(root);

        Scene scene = new Scene(root);
        scene.setFill(Color.TRANSPARENT);

        try {
            stage.getIcons()
                 .add(new Image(new FileInputStream("./res/icon_asafi"
                                                    + ".png")));
        } catch (FileNotFoundException e) { e.printStackTrace(); }
        stage.setScene(scene);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.setResizable(false);
        stage.setHeight(650);
        stage.setWidth(450);
        stage.setTitle("Connexion");
        stage.show();

        makeDraggable();
        connexionView();

        controllerStartWindow = new ControllerStartWindow(this);
    }

    /**
     * Setting up the different elements of the view.
     */
    private void connexionView() {

        Font defaultFont = Font.font("Segoe UI", FontPosture.REGULAR, 14);

        Text chooseAccount = new Text("Choisir un compte :");
        chooseAccount.setFont(defaultFont);

        userChooser = new ChoiceBox<>();
        userChooser.setPrefWidth(150);
        userChooser.setDisable(true);

        Text username = new Text("Nom d'utilisateur :");
        username.setFont(defaultFont);

        nameField = new TextField();

        Text password = new Text("Mot de passe :");
        password.setFont(defaultFont);

        passField = new PasswordField();

        GridPane grid = new GridPane();
        grid.add(chooseAccount, 0, 0);
        grid.add(userChooser, 1, 0);
        grid.add(username, 0, 1);
        grid.add(nameField, 1, 1);
        grid.add(password, 0, 2);
        grid.add(passField, 1, 2);
        grid.setAlignment(Pos.CENTER);
        grid.setVgap(7);
        grid.setHgap(45);

        connectBtn = new Button("Se connecter");
        connectBtn.setFont(defaultFont);
        connectBtn.setDisable(true);

        VBox connectElements = new VBox(grid, connectBtn);
        connectElements.setSpacing(25);
        connectElements.setAlignment(Pos.CENTER);

        ImageView title = null;
        try {
            title = new ImageView(new Image(new FileInputStream(
                    "./res/asafi_title" + ".png")));
        } catch (FileNotFoundException e) { e.printStackTrace(); }
        assert title != null;
        title.setPreserveRatio(true);
        title.setFitWidth(320);

        Text explain = new Text("Automation and Statistical Analysis For "
                                + "Instagram");
        explain.setFont(Font.font("Segoe UI", FontPosture.ITALIC, 13));
        explain.setTextAlignment(TextAlignment.CENTER);

        Text version = new Text("V1.0b");
        version.setTextAlignment(TextAlignment.CENTER);
        version.setFont(defaultFont);

        VBox head = new VBox(title, explain, version);
        head.setAlignment(Pos.CENTER);
        head.setSpacing(7);

        connexionProgress = new ProgressBar(0.75);
        connexionProgress.setMaxWidth(315);
        connexionProgress.setOpacity(0);
        connexionProgress.setProgress(0);

        info = new Text();
        info.setFont(defaultFont);

        VBox progressInfo = new VBox(connexionProgress, info);
        progressInfo.setAlignment(Pos.CENTER);
        progressInfo.setSpacing(5);

        exitBtn = new Button("Quitter");
        exitBtn.setFont(defaultFont);

        VBox container = new VBox(head, connectElements, progressInfo, exitBtn);
        container.setSpacing(70);
        container.setAlignment(Pos.CENTER);

        innerRoot.setCenter(container);

        eventSetup();
    }

    /**
     * Setting up the different events in the view.
     */
    private void eventSetup() {

        userChooser.valueProperty()
                   .addListener((observableValue, s, t1) -> controllerStartWindow
                           .accountChosenEvent(userChooser.getValue()));

        nameField.textProperty()
                 .addListener((observableValue, s, t1) -> connectBtn.setDisable(
                         t1.isBlank() || passField.getText().isBlank()));
        passField.textProperty()
                 .addListener((observableValue, s, t1) -> connectBtn.setDisable(
                         t1.isBlank() || nameField.getText().isBlank()));

        connectBtn.setOnAction(e -> {
            controllerStartWindow.connectTask(nameField.getText(),
                                              passField.getText());
            nameField.setDisable(true);
            passField.setDisable(true);
            connectBtn.setDisable(true);
        });

        exitBtn.setOnAction(e -> {
            controllerStartWindow.exit();
            stage.close();
        });
    }

    /**
     * Makes the window movable with the mouse over its entire surface.
     */
    private void makeDraggable() {

        AtomicReference<Double> gapX = new AtomicReference<>((double) 0);
        AtomicReference<Double> gapY = new AtomicReference<>((double) 0);

        innerRoot.setOnMouseMoved(e -> {


            gapX.set(e.getScreenX() - stage.getX());
            gapY.set(e.getScreenY() - stage.getY());
        });

        innerRoot.setOnMouseDragged(e -> {
            stage.setX(e.getScreenX() - gapX.get());
            stage.setY(e.getScreenY() - gapY.get());
        });
    }

    /**
     * If the connection fails, the elements are reset to a state to allow a new
     * connection.
     */
    public void connexionAborted() {

        nameField.setDisable(false);
        passField.setDisable(false);
        passField.clear();
        userChooser.setDisable(!(userChooser.getItems().size() == 0));
    }

    /**
     * Indicates connection progress status.
     *
     * @param text     the new text
     * @param progress the new value
     */
    public void setProgress(String text, double progress) {

        info.setText(text);
        double currProgress = connexionProgress.getProgress();
        Transition scaleTransition = new Transition() {
            {
                setCycleDuration(Duration.millis(TRANSITION_PROGRESS_TIME));
                setInterpolator(Interpolator.EASE_BOTH);
            }

            @Override
            protected void interpolate(double v) {
                connexionProgress.setProgress(currProgress
                                              + (progress - currProgress) * v);
            }
        };
        scaleTransition.play();
    }

    /** @return the nameField */
    public TextField getNameField() { return nameField; }

    /** @return the passField */
    public PasswordField getPassField() { return passField; }

    /** @return the stage */
    public Stage getStage() { return stage; }

    /**
     * Indicate here whether the progress bar is visible or not.
     *
     * @param isVisible if true, the bar will be made visible
     */
    public void setProgressVisible(boolean isVisible) {
        connexionProgress.setOpacity(isVisible ? 1 : 0);
        info.setOpacity(isVisible ? 1 : 0);
    }

    /**
     * Sets the list of accounts offered to the user.
     *
     * @param userLists user list
     */
    public void setListAccount(List<String> userLists) {
        userChooser.setItems(FXCollections.observableList(userLists));
        userChooser.setDisable(userLists.size() == 0);
    }
}