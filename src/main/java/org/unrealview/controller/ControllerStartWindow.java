/*
 * ControllerStartWindow.java                    date: 11/05/2020 22:41
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.controller;

import main.java.org.unrealview.model.GlobalSettings;
import main.java.org.unrealview.model.automation.Browser;
import main.java.org.unrealview.model.automation.search.UrlKey;
import main.java.org.unrealview.model.data.Reader;
import main.java.org.unrealview.model.data.Writer;
import main.java.org.unrealview.model.exception.InternalException;
import main.java.org.unrealview.vue.main.MainWindow;
import main.java.org.unrealview.vue.start.StartWindow;

import java.util.ArrayList;

import javafx.concurrent.Service;
import javafx.concurrent.Task;

/**
 * Startup window controller.
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class ControllerStartWindow {

    /** Browser to allow connection attempts. */
    private Browser browser;

    /** The username for the login attempt. */
    private String username;

    /** The password for the login attempt. */
    private String password;

    /** Lists of potentially existing account names. */
    private ArrayList<String> existingAccount;

    /** The startup window. */
    private StartWindow startWindow;

    /**
     * Uses file reading to search for registered accounts.
     *
     * @param startWindow the startup window
     */
    public ControllerStartWindow(StartWindow startWindow) {

        this.startWindow = startWindow;
        existingAccount = (ArrayList<String>) Reader.findAccounts();
        if (existingAccount == null) { existingAccount = new ArrayList<>(); }
        startWindow.setListAccount(existingAccount);
    }    /** The connection service. */
    private final Service<Void> connector = new Service<>() {

        @Override
        protected void succeeded() { startMainGUI(); }

        @Override
        protected Task<Void> createTask() {

            return new Task<>() {

                @Override
                protected Void call() {
                    connect();
                    return null;
                }
            };
        }
    };

    /**
     * Login procedure keeping the user informed with the interface of its
     * status.
     */
    private void connect() {

        startWindow.setProgressVisible(true);
        browser = new Browser(username, password);
        startWindow.setProgress("Test d'accès Internet", 0.1);
        if (!browser.pingUrl(UrlKey.INTERNET)) {
            fail("Pas d'accès Internet.");
            return;
        }
        startWindow.setProgress("Test d'accès instagram.com", 0.3);
        if (!browser.pingUrl(UrlKey.INSTAGRAM)) {
            fail("Instagram est inaccessible.");
            return;
        }
        startWindow.setProgress("Lancement de Chrome", 0.5);
        if (!browser.launch()) {
            fail("Échec du lancement de Chrome.");
            return;
        }
        startWindow.setProgress("Connexion de l'utilisateur", 0.75);
        try {
            if (!browser.connect()) {
                fail("La connexion à échouée.");
                return;
            }
        } catch (InternalException e) {
            e.printStackTrace();
        }
        startWindow.setProgress("Connecté.", 1);
        browser.exit();
    }

    /**
     * In case of failure an explicit message is displayed.
     *
     * @param info the message
     */
    private void fail(String info) {

        startWindow.setProgress(info, 0);
        startWindow.connexionAborted();
        browser.exit();
        connector.cancel();
    }

    /**
     * Starting the main window.
     */
    private void startMainGUI() {

        GlobalSettings globalSettings;
        if (existingAccount.contains(username)) {
            globalSettings = Reader.readSettings(username);
            if (!globalSettings.getPassword().equals(password)) {
                globalSettings.setPassword(password);
                Writer.saveSettings(globalSettings, username);
            }
        } else {
            globalSettings = new GlobalSettings();
            globalSettings.setUsername(username);
            globalSettings.setPassword(password);
            Writer.initAccount(username);
            Writer.saveSettings(globalSettings, username);
        }

        startWindow.getStage().close();
        new MainWindow();
    }

    /**
     * When selecting a registered account, the user's settings are checked to
     * see if the password is prompted or pre-entered.
     *
     * @param username the username for read the setting file
     */
    public void accountChosenEvent(String username) {

        startWindow.getNameField().setText(username);
        startWindow.getNameField().setDisable(true);
        startWindow.getPassField().requestFocus();

        GlobalSettings globalSettings = Reader.readSettings(username);

        if (!globalSettings.isRequirePassAtStart()) {
            startWindow.getPassField().setText(globalSettings.getPassword());
            startWindow.getPassField().setDisable(true);
        }
    }

    /**
     * Parameters the identifiers and launches the connection procedure.
     *
     * @param username the username
     * @param password the password
     */
    public void connectTask(String username, String password) {

        this.username = username;
        this.password = password;
        connector.restart();
    }

    /**
     * Closes the possible driver and quits the application.
     */
    public void exit() {

        connector.cancel();
        if (browser != null) {
            browser.exit();
        }
    }


}
