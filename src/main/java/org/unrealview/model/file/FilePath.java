/*
 * FilePath.java                                 date: 09/05/2020 17:39
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */


package main.java.org.unrealview.model.file;

import java.nio.file.FileSystems;

/**
 * Windows standard application file path.
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public final class FilePath {

    public static final String YML_EXT = ".yml";

    public static final String TSV_EXT = ".tsv";

    public static final String TSV_SEP = "\t";

    /** The path to the origin of the account folders. */
    public static final String ROOT = "config";

    public static final String SETTINGS = "global_settings.yml";

    /** The path to the configuration files of the robot execution profiles. */
    public static final String PROFILES = "profiles";

    public static final String LOGS = "logs";

    public static final String DATA = "data";

    public static final String BY_LIST = "by_list";

    public static final String ONE_BY_ONE = "one_by_one";

    public static final String STATS = "statistics";

    public static String root = ".";

    public static String sep = FileSystems.getDefault().getSeparator();
}
