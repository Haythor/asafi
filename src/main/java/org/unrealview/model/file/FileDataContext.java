/*
 * FileDataContext.java                          date: 09/08/2021 13:05
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */


package main.java.org.unrealview.model.file;

import main.java.org.unrealview.model.automation.object.InstaObj;
import main.java.org.unrealview.model.automation.object.Post;
import main.java.org.unrealview.model.automation.object.User;

import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO describe this class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class FileDataContext {

    public static final String SEP_NAME = "_";


    private HashMap<Instant, HashMap<String, ArrayList<User>>> usersByLists;


    private HashMap<String, HashMap<Instant, Post>> postsOneByOne;

    private HashMap<String, HashMap<Instant, User>> usersOneByOne;

    public FileDataContext() {

        usersByLists = new HashMap<>();

        postsOneByOne = new HashMap<>();
        usersOneByOne = new HashMap<>();
    }

    /**
     * TODO describe this method
     */
    public void writeAll() {
        try {
            Writer.writeFdc(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * TODO describe this method
     */
    public void addEntryForUsersLst(String fromName, String category,
                                    User usr) {

        Instant instant  = Instant.now().truncatedTo(ChronoUnit.DAYS);
        String  filename = category + SEP_NAME + fromName;

        HashMap<String, ArrayList<User>> listHashMap =
                usersByLists.get(instant);

        ArrayList<User> newList = new ArrayList<>() {{ add(usr); }};

        if (listHashMap == null) {
            HashMap<String, ArrayList<User>> newMap = new HashMap<>();
            newMap.put(filename, newList);
            usersByLists.put(instant, newMap);
        } else {
            ArrayList<User> users = listHashMap.get(filename);
            if (users == null) {
                listHashMap.put(filename, newList);
            } else {
                users.add(usr);
            }
        }
    }

    /**
     * TODO describe this method
     */
    public void addEntryForObO(User user, String name) {

        HashMap<Instant, User> map = usersOneByOne.get(name);

        if (map == null) { map = new HashMap<>(); }

        map.put(Instant.now(), user);
        usersOneByOne.put(name, map);
    }

    /**
     * TODO describe this method
     */
    public void deleteEntryForObO(User user, String name) {

        HashMap<Instant, User> map = usersOneByOne.get(name);

        if (map == null) { return; }

        for (Map.Entry<Instant, User> entry : map.entrySet()) {
            User usr = entry.getValue();
            if (usr.equals(user)) {
                map.remove(entry.getKey(), usr);
                break;
            }
        }
    }

    /**
     * TODO describe this method
     */
    public void addEntryForObO(Post post, String name) {

        HashMap<Instant, Post> map = postsOneByOne.get(name);

        if (map == null) { map = new HashMap<>(); }

        map.put(Instant.now().truncatedTo(ChronoUnit.SECONDS), post);
        postsOneByOne.put(name, map);
    }

    /**
     * TODO describe this method
     */
    public void deleteEntryForObO(Post post, String name) {

        HashMap<Instant, Post> map = postsOneByOne.get(name);

        if (map == null) { return; }

        for (Map.Entry<Instant, Post> entry : map.entrySet()) {
            Post pst = entry.getValue();
            if (pst.equals(post)) {
                map.remove(entry.getKey(), pst);
                break;
            }
        }
    }

    /**
     * TODO describe this method
     */
    public void addStatEntry(InstaObj obj) {

        if (obj instanceof User) {
            try {
                Writer.stat((User) obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (obj instanceof Post) {
            try {
                Writer.stat((Post) obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            throw new IllegalStateException(
                    "Unexpected object for statistic entry: " +
                    obj.getClass().getSimpleName());
        }
    }

    /** @return the postsOneByOne */
    public HashMap<String, HashMap<Instant, Post>> getPostsOneByOne() {
        return postsOneByOne;
    }

    /** @param postsOneByOne the postsOneByOne to set */
    public void setPostsOneByOne(
            HashMap<String, HashMap<Instant, Post>> postsOneByOne) {
        this.postsOneByOne = postsOneByOne;
    }

    /** @return the usersOneByOne */
    public HashMap<String, HashMap<Instant, User>> getUsersOneByOne() {
        return usersOneByOne;
    }

    /** @param usersOneByOne the usersOneByOne to set */
    public void setUsersOneByOne(
            HashMap<String, HashMap<Instant, User>> usersOneByOne) {
        this.usersOneByOne = usersOneByOne;
    }

    /** @return the usersByLists */
    public HashMap<Instant, HashMap<String, ArrayList<User>>> getUsersByLists() {
        return usersByLists;
    }

    /** @param usersByLists the usersByLists to set */
    public void setUsersByLists(
            HashMap<Instant, HashMap<String, ArrayList<User>>> usersByLists) {
        this.usersByLists = usersByLists;
    }
}
