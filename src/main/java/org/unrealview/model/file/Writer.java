/*
 * Writer.java                                   date: 03/05/2020 21:07
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.file;

import main.java.org.unrealview.model.automation.object.Post;
import main.java.org.unrealview.model.automation.object.User;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static main.java.org.unrealview.model.file.FilePath.*;

/**
 * Set of methods to allow writing of the different files of the application.
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public final class Writer {

    /**
     * TODO describe this method
     */
    public static void stat(Post post) throws IOException {

        Path path = Path.of(root +
                            sep +
                            ROOT +
                            sep +
                            DATA +
                            sep +
                            STATS +
                            sep +
                            post.getStringValues().get("shortcode") +
                            TSV_EXT);

        String content = Instant.now().truncatedTo(ChronoUnit.SECONDS) +
                         TSV_SEP +
                         post.getIntValues().get("like amount") +
                         TSV_SEP +
                         post.getIntValues().get("comment amount") +
                         "\n";

        if (Files.notExists(path)) {
            Files.createFile(path);
            content = "timestamp" +
                      TSV_SEP +
                      "like amount" +
                      TSV_SEP +
                      "comment amount\n" +
                      content;
        }

        Files.writeString(path, content, StandardOpenOption.APPEND);
    }

    /**
     * TODO describe this method
     */
    public static void stat(User user) throws IOException {

        Path path = Path.of(root +
                            sep +
                            ROOT +
                            sep +
                            DATA +
                            sep +
                            STATS +
                            sep +
                            user.getStringValues().get("username") +
                            TSV_EXT);

        String content = Instant.now().truncatedTo(ChronoUnit.SECONDS) +
                         TSV_SEP +
                         user.getIntValues().get("followers amount") +
                         TSV_SEP +
                         user.getIntValues().get("following amount") +
                         TSV_SEP +
                         user.getIntValues().get("post amount") +
                         "\n";

        if (Files.notExists(path)) {
            Files.createFile(path);
            content = "timestamp" +
                      TSV_SEP +
                      "followers amount" +
                      TSV_SEP +
                      "following amount" +
                      TSV_SEP +
                      "post amount\n" +
                      content;
        }

        Files.writeString(path, content, StandardOpenOption.APPEND);
    }

    /**
     * TODO describe this method
     */
    public static void writeFdc(FileDataContext fdc) throws IOException {

        Path dataDir = Path.of(root + sep + ROOT + sep + DATA);

        for (Map.Entry<Instant, HashMap<String, ArrayList<User>>> e :
                fdc.getUsersByLists()
                                                                         .entrySet()) {
            Instant                          instant = e.getKey();
            HashMap<String, ArrayList<User>> map     = e.getValue();

            Path dirDay = Path.of(dataDir + sep + BY_LIST + sep + instant);
            if (Files.notExists(dirDay)) { Files.createDirectory(dirDay); }

            for (Map.Entry<String, ArrayList<User>> entry : map.entrySet()) {

                Path curFile = Path.of(dirDay + sep + entry.getKey() + TSV_EXT);

                if (Files.exists(curFile)) { Files.delete(curFile); }

                Files.createFile(curFile);

                for (User user : entry.getValue()) {
                    Files.writeString(curFile,
                                      user.getStringValues().get("username") +
                                      "\n",
                                      StandardOpenOption.APPEND);
                }
            }
        }

        for (Map.Entry<String, HashMap<Instant, User>> e :
                fdc.getUsersOneByOne()
                                                              .entrySet()) {

            Path usersDir = Path.of(dataDir + sep + ONE_BY_ONE + sep + "users");
            if (Files.notExists(usersDir)) { Files.createDirectory(usersDir); }

            Path curFile = Path.of(usersDir + sep + e.getKey() + TSV_EXT);

            if (Files.exists(curFile)) { Files.delete(curFile); }

            Files.createFile(curFile);

            for (Map.Entry<Instant, User> entry : e.getValue().entrySet()) {

                Instant instant = entry.getKey();
                User    user    = entry.getValue();

                Files.writeString(curFile,
                                  instant +
                                  TSV_SEP +
                                  user.getStringValues().get("username") +
                                  "\n",
                                  StandardOpenOption.APPEND);
            }
        }

        for (Map.Entry<String, HashMap<Instant, Post>> e :
                fdc.getPostsOneByOne()
                                                              .entrySet()) {

            Path postsDir = Path.of(dataDir + sep + ONE_BY_ONE + sep + "posts");
            if (Files.notExists(postsDir)) { Files.createDirectory(postsDir); }

            Path curFile = Path.of(postsDir + sep + e.getKey() + TSV_EXT);

            if (Files.exists(curFile)) { Files.delete(curFile); }

            Files.createFile(curFile);

            for (Map.Entry<Instant, Post> entry : e.getValue().entrySet()) {

                Instant instant = entry.getKey();
                Post    user    = entry.getValue();

                Files.writeString(curFile,
                                  instant +
                                  TSV_SEP +
                                  user.getStringValues().get("shortcode") +
                                  "\n",
                                  StandardOpenOption.APPEND);
            }
        }
    }
}