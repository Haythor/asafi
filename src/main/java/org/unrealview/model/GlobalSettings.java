/*
 * GlobalSettings.java                           date: 03/05/2020 21:09
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model;

import main.java.org.unrealview.model.schedule.ProfilePlan;

import java.util.HashMap;
import java.util.List;

/**
 * TODO describe this class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class GlobalSettings {

    public List<ProfilePlan> profilePlans;

    /** Maximum amount of like per hour. */
    public int likeMaxAmount;

    /** Maximum amount of follow per hour. */
    public int followMaxAmount;

    /** Maximum amount of comment per hour. */
    public int commentsMaxAmount;

    /** List of comment lists available. */
    public HashMap<String, List<String>> comments;

    public GlobalSettings() {
        likeMaxAmount = 100;
        followMaxAmount = 75;
        commentsMaxAmount = 25;
    }
}
