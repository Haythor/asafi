/*
 * User.java                                     date: 07/05/2020 20:59
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation.object;

import java.util.HashMap;
import java.util.Map;

/**
 * Representation of a user.
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class User extends InstaObj {


    public User(Map<String, Boolean> boolValues, Map<String, Integer> intValues,
                Map<String, String> stringValues) {
        this.boolValues = boolValues;
        this.intValues = intValues;
        this.stringValues = stringValues;

        LOGGER.debug(this);
    }

    public User(String username) {
        stringValues = new HashMap<>();
        stringValues.put("username", username);

        LOGGER.debug("New simple user: '{}'", username);
    }

    @Override
    public String toString() { return "User " + super.toString(); }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof User &&
               ((User) obj).getStringValues()
                           .get("username")
                           .equals(stringValues.get("username"));
    }
}
