/*
 * Post.java                                     date: 07/05/2020 21:00
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation.object;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

/**
 * Representation of a user's post.
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class Post extends InstaObj {

    /** Date of publication. */
    private Instant publishInstant;

    /** The owner of this post. */
    private User owner;

    public Post(String shortcode) {
        stringValues = new HashMap<>();
        stringValues.put("shortcode", shortcode);
        LOGGER.debug("new simple post: '{}'", shortcode);
    }

    public Post(Map<String, Boolean> boolValues, Map<String, Integer> intValues,
                Map<String, String> stringValues, Instant publishInstant,
                User owner) {
        this.boolValues = boolValues;
        this.intValues = intValues;
        this.stringValues = stringValues;
        this.publishInstant = publishInstant;
        this.owner = owner;

        LOGGER.debug(this);
    }

    @Override
    public String toString() {

        return "Post{publishInstant=" +
               publishInstant +
               ", owner=" +
               owner +
               "} " +
               super.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Post &&
               ((Post) obj).getStringValues()
                           .get("shortcode")
                           .equals(stringValues.get("shortcode"));
    }

    /** @return the publishDate */
    public Instant getPublishInstant() { return publishInstant; }

    /** @return the owner */
    public User getOwner() { return owner; }
}
