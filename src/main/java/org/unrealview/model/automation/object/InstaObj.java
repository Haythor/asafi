/*
 * InstaObj.java                                 date: 09/16/2021 10:01
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation.object;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

/**
 * TODO describe this class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public abstract class InstaObj {

    protected static final Logger LOGGER = LogManager.getLogger("main");

    /** TODO describe this attribute */
    protected  Map<String, Integer> intValues;

    /** TODO describe this attribute */
    protected  Map<String, Boolean> boolValues;

    /** TODO describe this attribute */
    protected  Map<String, String> stringValues;

    @Override
    public String toString() {
        return "{" +
               "intValues=" +
               intValues +
               ", boolValues=" +
               boolValues +
               ", stringValues=" +
               stringValues +
               '}';
    }

    /** @return the boolValues */
    public Map<String, Boolean> getBoolValues() { return boolValues; }

    /** @return the intValues */
    public Map<String, Integer> getIntValues() { return intValues; }

    /** @return the stringValues */
    public Map<String, String> getStringValues() { return stringValues; }
}
