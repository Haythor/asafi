/*
 * BrowseFollowersOfUser.java                  date: 09/16/2021 11:27
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation.task.browse;

import main.java.org.unrealview.model.automation.object.User;
import main.java.org.unrealview.model.exception.InternalException;

import java.util.Optional;

/**
 * TODO describe this class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class BrowseFollowersOfUser extends BrowseTsk {

    public BrowseFollowersOfUser() { super(); }

    @Override
    protected void fillList() {

        User usr;
        if (!value.isEmpty()) {
            try {
                Optional<User> optUsr = interpreter.browser.obtainUser(value);
                if (optUsr.isPresent()) {
                    usr = optUsr.get();
                } else {
                    LOGGER.warn("Can't obtain user with this value: {}. " +
                                "BrowseFollowersOfUser cancelled.", value);
                    return;
                }
            } catch (InternalException e) {
                LOGGER.error("Something wrong with obtainUser.");
                LOGGER.debug(e.getMessage());
                return;
            }
        } else {
            if (current == null || !(current instanceof User)) {
                LOGGER.warn("No input value or previous user for " +
                            "BrowseFollowersOfUser.");
                return;
            }
            usr = (User) current;
        }

        try {
            results.addAll(interpreter.browser.grabListFollowers(usr));
            LOGGER.info("BrowseFollowersOfUser now has {} values.",
                        results.size());
        } catch (InternalException e) {
            LOGGER.error("Something wrong with grabListFollowers.");
            LOGGER.debug(e.getMessage());
        }
    }
}
