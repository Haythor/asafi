/*
 * PostFilter.java                               date: 06/05/2020 18:25
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation.task.filter;

import main.java.org.unrealview.model.automation.object.Post;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static main.java.org.unrealview.model.automation.Property.DEFAULT_NUMERIC_VALUE;

/**
 * Post filter settings.
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class PostFilter extends InstaObjFilter {

    /**
     * Duration in days subtracted from the date of publication. Check if
     * publish date is before.
     */
    private final long before;

    /**
     * Duration in days subtracted from the date of publication. Check if
     * publish date is after.
     */
    private final long after;

    public PostFilter() {
        before = DEFAULT_NUMERIC_VALUE;
        after = DEFAULT_NUMERIC_VALUE;
    }


    /**
     * Tests each filter point that are set up.
     *
     * @return true if the post passes the filter
     */
    public boolean isMatch() {

        if (current == null || !(current instanceof Post)) { return false; }

        Post post = (Post) current;
        String sc = post.getStringValues().get("shortcode");

        if (!super.preMatch(post, sc)) { return false; }

        String description = post.getStringValues().get("description");

        String regexInclude = stringValues.get("description include pattern");
        if (regexInclude != null && !description.matches(regexInclude)) {
            LOGGER.info("The post ({}) does not match the filter. For " +
                        "'description include pattern' filter, regex is: {}, " +
                        "description: {}", sc, regexInclude, description);

            return false;
        }

        String regexExclude = stringValues.get("description exclude pattern");
        if (regexExclude != null && description.matches(regexExclude)) {
            LOGGER.info("The post ({}) does not match the filter. For " +
                        "'description exclude pattern' filter, regex is: {}, " +
                        "description: {}", sc, regexExclude, description);
            return false;
        }


        Instant publish = post.getPublishInstant();
        Instant now     = Instant.now();

        if (before != DEFAULT_NUMERIC_VALUE &&
            publish.isBefore(now.minus(before, ChronoUnit.DAYS))) {
            LOGGER.info("The post ({}) does not match the filter. For " +
                        "'after' filter, expected limit is: {}, current: {}",
                        sc,
                        after,
                        publish);
            return false;
        }

        if (after != DEFAULT_NUMERIC_VALUE &&
            publish.isAfter(now.minus(after, ChronoUnit.DAYS))) {
            LOGGER.info("The post ({}) does not match the filter. For " +
                        "'after' filter, expected limit is: {}, current: {}",
                        sc,
                        after,
                        publish);
            return false;
        }


        return true;
    }
}