/*
 * AsafiTsk.java                                date: 09/18/2021 17:46
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation.task;

import main.java.org.unrealview.model.automation.Interpreter;
import main.java.org.unrealview.model.automation.object.InstaObj;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * TODO describe this class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public abstract class AsafiTsk {

    protected static final Logger LOGGER = LogManager.getLogger("main");

    /** TODO describe this attribute */
    public Interpreter interpreter;

    /** TODO describe this attribute */
    protected InstaObj current;

    // public AsafiTsk() { }

    public void execute(InstaObj current) {
        this.current = current;
    }
}
