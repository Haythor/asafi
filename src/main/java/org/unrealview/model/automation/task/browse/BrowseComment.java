/*
 * BrowseComment.java                            date: 06/05/2020 18:22
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation.task.browse;

import main.java.org.unrealview.model.automation.task.filter.CommentFilter;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public class BrowseComment extends BrowseTsk {

    /**
     * The eventual filter has to be applied on the results to eliminate them.
     * As far as possible new elements will be sought to compensate.
     */
    private CommentFilter commentFilter;

    public BrowseComment() { }

    @Override
    protected void fillList() {

    }

    /** @return the commentFilter */
    public CommentFilter getCommentFilter() { return commentFilter; }

    /** @param commentFilter the commentFilter to set */
    public void setCommentFilter(CommentFilter commentFilter) {
        this.commentFilter = commentFilter;
    }
}