/*
 * HumanTsk.java                                 date: 16/06/2020 15:38
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation.task.insta;

import main.java.org.unrealview.model.automation.object.InstaObj;
import main.java.org.unrealview.model.automation.task.AsafiTsk;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO describe the class
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public abstract class HumanTsk extends AsafiTsk {


    /** Minimum waiting time before executing the task. */
    public long minWaitTime;

    /** Maximum waiting time before executing the task. */
    public long maxWaitTime;

    /** Probability of the task being performed. */
    public double probability;


    /** TODO describe this attribute */
    public List<AsafiTsk> tasks;

    public HumanTsk() {

        minWaitTime = 0;
        maxWaitTime = 0;
        probability = 1;

        tasks = new ArrayList<>();
    }

    /**
     * TODO describe this method
     */
    @Override
    public void execute(InstaObj current) {

        if (Math.random() > probability) { return; }

        try {
            Thread.sleep((long) (minWaitTime +
                                 (maxWaitTime - minWaitTime) * Math.random()));
        } catch (InterruptedException | IllegalArgumentException e) {
            LOGGER.error("Please check value for min and max WaitTime.");
            return;
        }

        super.execute(current);

        if (make()) {
            tasks.forEach(tsk -> {
                tsk.interpreter = interpreter;
                tsk.execute(current);
            });
        }
    }

    /**
     * TODO describe this method
     */
    public abstract boolean make();
}
