/*
 * QueryHash.java               date: 08/05/2020 00:35
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation.search;

/**
 * Hash to make GET http queries to fetch different data.
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public final class QueryHash {

    /** Hash to get the publications on a tag's page. */
    public static final String TAG_POST = "298b92c8d7cad703f7565aa892ede943";

    /** Hash to get the publications on an account page. */
    public static final String USER_POST = "8c2a529969ee035a5063f2fc8602a0fd";

    /** Hash to get the followers on an account page. */
    public static final String FOLLOWERS = "37479f2b8209594dde7facb0d904896a";

    /** Hash to get the followed on an account page. */
    public static final String FOLLOWED = "58712303d941c6855d4e888c5f0cd22f";

    /** Hash to get the accounts we like on a post page. */
    public static final String POST_LIKE = "d5d763b1e2acf209d62d22d184488e57";

    /** Hash to get comments on the page of a post. */
    public static final String POST_COMMENT =
            "bc3296d1ce80a24b1b6e40b1e72903f5";

    private QueryHash() { throw new IllegalStateException("Utility class"); }
}
