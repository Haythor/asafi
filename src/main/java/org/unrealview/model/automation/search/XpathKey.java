/*
 * XpathKey.java                                 date: 05/05/2020 21:29
 *
 * This file is part of ASAFI.
 *
 * ASAFI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ASAFI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ASAFI. If not, see <https://www.gnu.org/licenses/>.
 */

package main.java.org.unrealview.model.automation.search;

/**
 * The xpath allows you to obtain elements of a web page to obtain information
 * or perform actions.
 *
 * @author Michel Paronnaud
 * @version 1.0
 */
public final class XpathKey {

    /** TODO describe this attribute */
    public static final String[] COOKIES_BUTTON =
            {"/html/body/div[4]/div/div/button[1]",
             "/html/body/div[3]/div/div/button[1]"};

    /** Xpath to get the account entry item. */
    public static final String[] USERNAME_INPUT =
            {"/html/body/div[1]/section/main/article/div[2]/div[1]/div/form" +
             "/div/div[1]/div/label/input",
             "/html/body/div[1]/div/div/section/main/article/div[2]/div[1" +
             "]/div/form/div/div[1]/div/label/input"};

    /** Xpath to get the password entry item. */
    public static final String[] PASSWORD_INPUT =
            {"/html/body/div[1]/section/main/article/div[2]/div[1]/div/form" +
             "/div/div[2]/div/label/input",
             "/html/body/div[1]/div/div/section/main/article/div[2]/div[1" +
             "]/div/form/div/div[2]/div/label/input"};

    /** Xpath to get account login button. */
    public static final String[] CONNECT_BUTTON =
            {"/html/body/div[1]/section/main/article/div[2]/div[1]/div/form" +
             "/div/div[3]/button",
             "/html/body/div[1]/div/div/section/main/article/div[2]/div[1" +
             "]/div/form/div/div[3]/button"};

    /** Xpath to get message access button. */
    public static final String DM_BUTTON =
            "/html/body/div[1]/section/nav/div[2]/div/div/div[3]/div/div[2]/a";

    /** Xpath to get the follow user button. */
    public static final String[] FOLLOW_USER_BUTTON =
            {"/html/body/div[1]/section/main/div/header/section/div[1]/div[1" +
             "]/div/div/div/span/span[1]/button",
             "/html/body/div[1]/section/main/div/header/section/div[1]/div[2" +
             "]/div/div/div/span/span[1]/button"};

    /** Xpath to get the follow tag button. */
    public static final String FOLLOW_TAG_BUTTON =
            "/html/body/div[1]/section/main/header/div[2]/div[1]/button";

    /** Xpath to get the like button of a post. */
    public static final String LIKE_BUTTON =
            "/html/body/div[1]/section/main/div/div[1]/article/div[2]/section" +
            "[1]/span[1]/button";

    /** Xpath to get the date element of a post. */
    public static final String DATETIME_POST =
            "/html/body/div[1]/section/main/div/div[1]/article/div[2]/div[2" +
            "]/a/time";

    private XpathKey() { throw new IllegalStateException("Utility class"); }
}
