# ASAFI

> ASAFI (V0.0.89) - Automation, Scrapping, Analyzer For Instagram

Ce logiciel a pour but de fournir des outils pour automatiser des tâches et pour faire de la
récupération d'informations sur Instagram. Pour cela il s'appuie sur des requêtes CURL pour
exploiter l'API d'intagram directement.

- [ASAFI](#asafi)
  - [Présentation](#présentation)
    - [Principe général](#principe-général)
    - [Gestion des données](#gestion-des-données)
      - [Structure des fichiers](#structure-des-fichiers)
      - [Explication de l'organisation](#explication-de-lorganisation)
    - [Informations récupérables](#informations-récupérables)
  - [Les profiles](#les-profiles)
    - [HumanTsk](#humantsk)
      - [Like](#like)
      - [Unlike](#unlike)
      - [Follow](#follow)
      - [Unfollow](#unfollow)
      - [Block](#block)
      - [Unblock](#unblock)
      - [GetOwnerFromPost](#getownerfrompost)
      - [GetOwnerFromComment](#getownerfromcomment)
      - [Limiter](#limiter)
      - [ResetLimiter](#resetlimiter)
      - [SaveAsList](#saveaslist)
      - [SaveOneByOneUser](#saveonebyoneuser)
      - [SaveOneByOnePost](#saveonebyonepost)
    - [BrowseTsk](#browsetsk)
      - [BrowseComment](#browsecomment)
      - [BrowseFollowingOfUser](#browsefollowingofuser)
      - [BrowseFollowersOfUser](#browsefollowersofuser)
      - [BrowseListFromFile](#browselistfromfile)
      - [BrowseOneByOneUser](#browseonebyoneuser)
      - [BrowseOneByOnePost](#browseonebyonepost)
      - [BrowsePostFromTag](#browsepostfromtag)
      - [BrowsePostFromUser](#browsepostfromuser)
    - [FilterTsk](#filtertsk)
      - [InstaObjFilter](#instaobjfilter)
        - [PostFilter](#postfilter)
        - [PostFilter](#postfilter-1)
      - [ListFilter](#listfilter)
        - [UserInList](#userinlist)
        - [PostInList](#postinlist)
  - [Exemples](#exemples)
    - [like_by_tag](#like_by_tag)

## Présentation

### Principe général

L'application s'appuie sur un fichier de configuration générale, des profiles et des fichiers de
données. Les profiles permettent de mettre en relations les différentes tâches disponible entre
elles et de les paramétrer. Pour protéger le compte utilisé, les principales actions (like, comment,
follow) sont comptées pour vérifier que leur quantité ne dépasse pas les limites générales définies dans
la configuration.

### Gestion des données

#### Structure des fichiers

```tree
config
    global_settings.yml
    +---logs
    +---profiles
    |   follow_like_by_tag.yml
    |   unfollow_unfollowers.yml
    |   block_unfollowers.yml
    |   like_by_feed.yml
    \---data
        +---by_list
        |   +---2021-01-01T00:00:00Z
        |   |   followers_userA.tsv
        |   |   following_userA.tsv
        |   |   followers_userB.tsv
        |   |   likes_AbC1xYz23Ml.tsv
        |   |   likes_AbC2xYz23Ml.tsv
        |   \---2021-01-02T00:00:00Z
        |       followers_userA.tsv
        |       followers_userB.tsv
        |       likes_AbC1xYz23Ml.tsv
        |       likes_AbC2xYz23Ml.tsv
        +---one_by_one
        |   +---users
        |   |   [CustomName1].tsv
        |   \---posts
        |       [CustomName2].tsv
        \---statistics
            user1.tsv
            AbC1xYz23Ml.tsv
```

#### Explication de l'organisation

Le dossier **config** regroupe l'ensemble des dossiers et fichiers de l'application. S'il
n'est pas dans le même dossier que l'exécutable, son chemin devra être précisé en argument.

- *global_settings.yml* : Paramétrage général lu au démarrage de l'application. À priori, indique la
  fréquence de lancement des profiles. Ainsi il n'y aura pas d'options sur la ligne de commande.
  Potentiellement les opérations d'analyses statistiques (non prévues dans un premier temps) se feront
  par un autre point d'entré.
- **logs** : Il reste à déterminer si ceux-ci seront rotatifs ou journaliers, par exemple. Ils
  permettront de suivre l'activitée en cours au niveau INFO.
- **profiles** : Liste de fichiers de paramétrage des profiles d'exécutions.
- **data** : Structure de dossiers et de fichiers pour enregistrer et charger des données relatives
  à Instagram générer par les différentes tâches disponibles.

  - **by_list** :
    Le système by_list est conçu pour manipuler des listes complètes journalières.
    L'objectif est, par exemple, de pouvoir effectuer des comparaisons entre deux dates
    différentes sur des ensembles d'une même source, mais aussi éventuellement de pouvoir
    faire des analyses ultérieures.

    Les nom des listes (et des fichiers) suivent un format simple : **category_name** qui permet
    de les trier et de les retrouver facilement.

    Le nom des dossiers correspond à la date d'enregistrement ; il ne peux pas y avoir
    d'enregistrement multiple : les fichiers sont écrasé en cas de nouvel enregistrement.

    ```csv
    user1
    user2
    ```

    Le fichier contient la liste des objets, représentés chaqun sur une ligne par leur version
    courte (username pour un User et shortcode pour un Post).

  - **one_by_one** :
    Ce système permet, comme son nom l'indique, d'intéragir avec les objets un par un. Chaque
    fichier a un nom unique et chaque ligne comporte le timestamp d'ajout et la version
    courte de l'objet enregistré. Il est possible de charger une séquence d'objets en ne prenant
    en compte que ceux ayant été ajoutés après un certain timestamp.

    ```csv
    01-12-2020 12:00:00   user1
    01-12-2020 12:00:10   user2
    ```

  - **statistics** :
    Les fichiers de statistiques permettent de suivre l'évolution des variables importantes d'un
    objet.

    ```csv
    timestamp   post amount  following   followers
    01-12-2020 12:00:00   1   0   0
    02-12-2020 12:00:00   1   1   5
    ```

### Informations récupérables

> Informations qui pourront être extraites, dans le but de les utiliser au sein de l'application à des
> fins d'analyse, ou pour créer des règles dans les profils d'automatisation.

- **Sur les publications :**
  - Nombre de like
  - Liste de like
  - Nombre de commentaires
  - Liste des commentaires
  - Est une vidéo
  - Est un carrousel
  - Date et heure de publication
  - Texte de la description

- **Sur les profils :**
  - Nombre de publications
  - Nombre d'abonnés
  - Nombre d'abonnements
  - Liste des abonnés
  - Liste des abonnements
  - Est privé
  - Est business
  - Est vérifié
  - Texte de la description

## Les profiles

> Les profiles représentent un ensemble de tâches pour permettre des opérations spécifiques et
> contiennent les identifiants du compte à utiliser.
> Plusieurs types de tâches sont combinables, comme les outils de recherches (pour
> obtenir des objets), les filtres, et les tâches à appliquer sur les résultats de ces
> filtres.

Ce système est conçu pour être modulaire : on peux considérer chaque tâches comme êtant un nœud
dans un système nodale, qui reçoit un objet à traiter et exécute ensuite ses propres nœuds.

Ainsi de manière générale chaque tâche :

- Est une *AsafiTsk*, et ainsi :
- Possède une valeur courante
- A un méchanisme d'exécution

Voici la liste des tâches :

- *HumanTsk*
  - Like
  - Unlike
  - Follow
  - Unfollow
  - Block
  - Unblock
  - GetOwnerFromPost
  - GetOwnerFromComment
  - Limiter
  - ResetLimiter
  - SaveAsList
  - SaveAsObOUser
  - SaveAsObOPost
- *BrowseTsk*
  - BrowseComment
  - BrowseFollowersOfUser
  - BrowseFollowingOfUser
  - BrowseListFromFile
  - BrowseOneByOneUser
  - BrowseOneByOnePost
  - BrowsePostFromTag
  - BrowsePostFromUser
- *FilterTsk*
  - *InstaObjFilter*
    - PostFilter
    - UserFilter
    - CommentFilter
  - *ListFilter*
    - UserInList
    - PostInList

### HumanTsk

- *Paramètres :*
  - **minWaitTime** : Temps d'attente minimal en secondes. (optional, default = 0)
  - **maxWaitTime** : Temps d'attente maximal en secondes. (optional, default = 0)
  - **probability** : Probilité d'exécution de la tâche. (optional, default = 1.0)
  - **tasks** : Listes de tâches. (optional, default = {})
- *Méthode d'action **make** :*
  Effectue une action concrète, éventuellemt à l'aide de la valeur courante. Renvoie le succès ou
  l'échec de sa procédure.
- *Méchanisme d'exécution :*
  La tâche s'effectue selon une certaine probabilité. Le temps d'attente est choisi aléatoirement
  entre les deux bornes fournies. Si **make** réussi, les tâches contenues dans **tasks** seront
  exécutées.

#### Like

Effectue l'action instagram like. La valeur courante doit être Post.

#### Unlike

Effectue l'action instagram unlike. La valeur courante doit être Post.

#### Follow

Effectue l'action instagram follow. La valeur courante doit être User.

#### Unfollow

Effectue l'action instagram unfollow. La valeur courante doit être User.

#### Block

Effectue l'action instagram block. La valeur courante doit être User.

#### Unblock

Effectue l'action instagram unblock. La valeur courante doit être User.

#### GetOwnerFromPost

Récupère l'utilisateur propriétaire de la valeur courante. La valeur courante doit être un Post.

#### GetOwnerFromComment

Récupère l'utilisateur propriétaire de la valeur courante. La valeur courante doit être un Comment.

#### Limiter

Limiter le nombre d'exécutions des sous tâches.

Paramètres :

- **amount** : Nombre d'exécution maximum. (obligatory)

#### ResetLimiter

Reinitialiser le compteur d'un Limiter.

Paramètres :

- **limiter** : La référence vers le Limiter. (obligatory)

#### SaveAsList

La valeur courante doit être User.

Paramètres :

- **username** : le nom de l'utilisateur source. (obligatory)
- **category** : la categorie de la source. (obligatory)

#### SaveOneByOneUser

La valeur courante doit être User.

Paramètres :

- **name** : le nom de la liste. (obligatory)

#### SaveOneByOnePost

La valeur courante doit être Post.

Paramètres :

- **name** : le nom de la liste. (obligatory)

### BrowseTsk

- *Paramètres :*
  - **value** : Selon la tâche, chaîne utilisée comme point de recherche. (possibly obligatory,
    default = "")
  - **path** : Méthode de parcours de la liste de résultats ; valeurs possibles : *FIFO*, *LIFO*, *
    RANDOM*. (optional, default = FIFO)
  - **amount** : Quantité de résultat maximum attendue ; ne fonctionne pas partout. (possibly
    obligatory, default = -1)
  - **tasks** : Listes de tâches. (optional, default = {})
- *Méthode d'action **fillList** :*
  Méthode de remplissage de la liste de résultat. En cas d'échec la liste sera simplement vide et de
  ce fait, aucune sous tâches ne sera exécutées.
- *Méchanisme d'exécution :*
  La tâche commence par demander le remplissage de la liste de résultats à **fillList**. En fonction
  de la valeur de **path**, la liste est réorganisée. Pour chaque objet dans celle-ci, chaque
  tâche est exécutée avec l'objet comme valeur courante.

#### BrowseComment

Recherche les commentaires à partir d'un Post. La valeur courante doit être Post ou **value** doit
être paramétré avec un shortcode.

#### BrowseFollowingOfUser

Recherche les following à partir d'un User. La valeur courante doit être User ou **value** doit être
paramétré avec un username.

#### BrowseFollowersOfUser

Recherche les followers à partir d'un User. La valeur courante doit être User ou **value** doit être
paramétré avec un username.

#### BrowseListFromFile

Charge une liste d'utilisateurs, enregistré dans un fichier de type *by_list*. Le paramètre **value**
et la valeur courante ne sont pas utilisés.

Paramètres :

- **username** : (obligatory)
- **category** : (obligatory)
- **day** : nombre de jours avant la date actuelle. (optional, default = 0)

#### BrowseOneByOneUser

Charge une liste de User depuis un fichier *one_by_one* compris dans un intervalle de temps (par
défaut tous les éléments sont chargés). Le paramètre **value**  et la valeur courante ne sont pas
utilisés.

Paramètres :

- **name** : nom de la liste. (obligatory)
- **before** : temps en minutes avant maintenant pour lequel l'entrée doit être antérieur. (
  optional, default = -1)
- **after** : temps en minutes avant maintenant pour lequel l'entrée doit être postérieur. (
  optional, default = -1)

#### BrowseOneByOnePost

Charge une liste de Post depuis un fichier *one_by_one* compris dans un intervalle de temps (par
défaut tous les éléments sont chargés). Le paramètre **value** et la valeur courante ne sont pas
utilisés.

Paramètres :

- **name** : nom de la liste. (obligatory)
- **before** : temps en minutes avant maintenant pour lequel l'entrée doit être antérieur. (
  optional, default = -1)
- **after** : temps en minutes avant maintenant pour lequel l'entrée doit être postérieur. (
  optional, default = -1)

#### BrowsePostFromTag

Recherche les Post à partir d'un tag instagram selon l'ordre d'affichage normal. Le paramètre **
value** doit être paramétré avec le nom du tag ; la valeur courante n'est pas utilisée.

#### BrowsePostFromUser

Recherche les Post à partir d'un User selon l'ordre d'affichage normal. La valeur courante doit être
User ou **value** doit être paramétré avec un username.

### FilterTsk

- *Paramètres :*
  - **tasksYes** : Listes de tâches à exécuter en cas du passage positif de la valeur courante. (
    optional, default = {})
  - **tasksNo** : Listes de tâches à exécuter en cas du passage négatif de la valeur courante. (
    optional, default = {})
- *Méthode d'action **isMatch** :*
  Les différents points de tests du filtres ; renvoie le succès ou l'échec.
- *Méchanisme d'exécution :*
  Selon le résultat de la méthode d'action, l'ensemble des sous tâches correspondantes sont exécutées.

#### InstaObjFilter

- *Paramètres :*
  - **intValues** :
  - **boolValues** :
  - **stringValues** :
- *Méchanisme d'exécution :*

##### PostFilter

Paramètres :

- **before**
- **after**
- **intValues**
  - **minimum like amount**
  - **maximum like amount**
  - **minimum comment amount**
  - **maximum comment amount**
- **boolValues**
  - **is video**
  - **comment disabled**
  - **comment disabled for viewer**
  - **is paid partnership**
  - **is affiliate**
  - **viewer has liked**
  - **viewer has saved**
  - **viewer has saved to collection**
  - **is ad**
- **stringValues**
  - **description exclude pattern**
  - **description include pattern**

##### PostFilter

Paramètres :

- **minRatio**
- **maxRatio**
- **intValues**
  - **minumum followers amount**
  - **maximum followers amount**
  - **minumum following amount**
  - **maximum following amount**
  - **minumum post amount**
  - **maximum post amount**
- **boolValues**
  - **is private**
  - **is business**
  - **is verified**
  - **avatar is setup**
- **stringValues**
  - **description exclude pattern**
  - **description include pattern**

#### ListFilter

- *Paramètres :*
  - **inputTasks** : Listes de tâches à exécuter pour remplir une liste de comparaison. Pour que le
    filtre puisse récupérer les résultats, il devra nécessairement se retrouver en sous tâche de
    l'une d'elles. (optional, default = {})
- *Méchanisme d'exécution :*
  Lors de sa première exécution les tâches **inputTasks** sont exécutées. Ensuite c'est la séquence
  normal du filtrage qui s'exécute.

##### UserInList

La valeur courante doit être User, sa présence est vérifiée dans la liste du filtre.

##### PostInList

La valeur courante doit être Post, sa présence est vérifiée dans la liste du filtre.

## Exemples

### like_by_tag

Cette configuration envisage une situation où un utilisateur cherche du contenu depuis un tag
donné. Ce profile permet de récupérer 50 posts depuis le tag pursuitofportraits. Pour chaque post, on
vérifie que son propriétaire n'a pas déjà été la cible du robot la semaine précédente (sous réserve
que la liste noire soit correctement utilisée), puis qu'il correspond à certain critères. Si tel
est le cas, trois posts sont prélevés au hasard de ses publications puis eventuellements "likés" et
commentés : dans ces cas, des nouvelles entrées sont ajoutées à la liste noire.

Le fichier de configuration générale *global_settings.yml* :

```yaml
!Settings

# likeMaxAmount: 100
# followMaxAmount: 75
# commentsMaxAmount: 25

profilePlans:
  - !profilePlan
    name: like_by_tag  # Le fichier du profile est like_by_tag.yml
    delay: 0           # Le profile sera exécuté immédiatement.
    repeat: 1          # Le profile sera exécuté une fois.

comments:
  my_default:
    - Hello world from ASAFI
    - And another one...
  cmt_multiple_likes:
    - Super!! 😀
    - Marvelous stuff 👏
    - I like your stuff 👌
    - Nice content!
    - Your work is awesome! 😍
    - Great images! 😍

```

Le fichier du profile *like_by_tag.yml* :

```yaml
!Profile

username:
password:

tasks:
  - !BrowsePostFromTag
    value: pursuitofportraits
    amount: 50
    tasks:
      - !GetOwnerFromPost
        minWaitTime: 15
        maxWaitTime: 30
        tasks:
          - &uil !UserInList
            inputTasks:
              - !BrowseOneByOneUser
                name: dynamic_blacklist
                before: 10080
                tasks:
                  - *uil
            tasksNo:
              - !UserFilter
                intValues:
                  minimum following amount: 50
                  maximum following amount: 2500
                  minimum followers amount: 180
                  maximum followers amount: 1200
                  minimum post amount: 9
                  maximum post amount: 500
                boolValues:
                  avatar is setup: true
                minRatio: 0.5
                maxRatio: 2.2
                tasksYes:
                  - !ResetLimiter
                    limiter: &rl !Limiter
                      amount: 3
                      tasks:
                        - !Like
                          probability: 0.3
                          minWaitTime: 2
                          maxWaitTime: 10
                          tasks:
                            - &gofp !GetOwnerFromPost
                              tasks:
                                - !SaveOneByOneUser
                                  name: dynamic_blacklist

                        - !Comment
                          source: cmt_multiple_likes
                          probability: 0.3
                          tasks:
                            - *gofp
                  - !BrowsePostFromUser
                    amount: 15
                    path: random
                    tasks:
                      - !PostFilter
                        intValues:
                          maximum like amount: 600
                          maximum comment amount: 50
                        stringValues:
                          description exclude pattern: (nsfw|#dog|#cat)
                        tasksYes:
                          - *rl

```

![like_by_tag-flowchart](./res/like_by_tag.png)
